import { Fragment, useEffect, useState } from "react"
import Course from "../components/Course"

// Boostrap Components
import Container from 'react-bootstrap/Container'

// Data Imports
// import courses from '../mock-data/courses'

const Courses = () =>{
    const [courses, setCourses] = useState()

    // Retrieves the courses from the database
    useEffect(()=>{
        fetch('http://localhost:4000/api/courses')
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setCourses(data.map(course =>{
                    return(
                        <CourseCard key={course._id} courseProp={course} />
                    )
                }))
            })
    },[])

    return(
        <Container fluid>
            {CourseCards}
        </Container>
    )
}

export default Courses