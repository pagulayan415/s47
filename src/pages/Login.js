// Base imports
import { useState, useEffect, useContext } from "react"
import { Navigate } from "react-router-dom"

// Boostrap Components
import Form from "react-bootstrap/esm/Form"
import Container from "react-bootstrap/esm/Container"
import Button from "react-bootstrap/esm/Button"

// App imports
import UserContext from "../UserContext"

const Login = () =>{
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);
    const {user, setUser} = useContext(UserContext)

useEffect(()=>{
    let isEmailNotEmpty = email !== '';
    let isPasswordNotEmpty = password !== '';

    if(isEmailNotEmpty && isPasswordNotEmpty === true){
        setIsDisabled(false)
    }else{
        setIsDisabled(true)
    }
},[email, password])

    const register = (e) =>{
        e.preventDefault()
        alert('You are now logged in')

        localStorage.setItem('email', email)

        setUser({email: email})
        console.log(user)

        setEmail('');
        setPassword('');

}

    if(user.email !== null){
        return <Navigate to="/" />
    }
    return(
        <Container fluid>
            <Form onSubmit={register}>
                <Form.Group>
                    <h1>Login</h1>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" required value={email} onChange={(e)=> setEmail(e.target.value)}></Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" required value={password} onChange={(e)=>setPassword(e.target.value)}></Form.Control>
                </Form.Group>
                <Button variant="success" type="submit" disabled={isDisabled}>Submit</Button>
            </Form>
        </Container>
    )
}

export default Login